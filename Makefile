CFLAGS  = -c #-Wall
CC      = gcc
DISTDIR = dist
LIBDIR  = lib
SLNDIR	= solution
OBJECTS = $(DISTDIR)/my-functions.o\
		  $(DISTDIR)/solution.o

link: $(OBJECTS) 
	$(CC) $? -o $(DISTDIR)/simulate

dist/solution.o: $(SLNDIR)/solution.c
	$(CC) $(CFLAGS) $? -o $(DISTDIR)/solution.o

dist/my-functions.o: $(LIBDIR)/my-functions.c
	$(CC) $(CFLAGS) $? -o $(DISTDIR)/my-functions.o

clean:
	rm -rf ./$(DISTDIR) && mkdir ./$(DISTDIR) #delete and re-make the dist file

run: link
	./$(DISTDIR)/simulate

#$? is the variable that the function needs i.e: dist/my-functions.o dist/solution.o