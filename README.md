# Memory Virtualization
Allocate a block of memory sufficient to store bytes for the full system address space (note: you may assume the
system will always have access to a quantity of physical memory equal to the full address space)

Create a Page Table for a single process (note: you are not required to model any other components of a process)

Randomly write between 2048 and 20480 bytes of data belonging to the ‘process’ previously created. Both the
content and locations of these bytes should be pseudo-randomised to some extent.

Write a file to [your solution]/data/physical_memory.txt which displays the contents of your simulation of
physical memory in linear, readable form. You must clearly label the memory addresses that are and are not used.

Write a file to [your solution]/data/page_table.txt which displays the contents of your simulated page table
in linear, readable form. 

Add 2 entries to the Page Table which point to pages which are not stored in your simulated physical memory, and
store content for these pages in your solution’s data folder (the naming convention is at your discretion). You
should print 1 virtual address from each of these pages, clearly labelled, to the console.

Print, to the console, a human-readable description of the structure of a single page table entry that you have
elected to use (note: you should also document your decisions regarding this in your README.md file)

The physical memory is 512 bytes. i store the hex value in the first byte and control bits in second byte
Example:    
			
			pM[0] = HEX1
            pM[1] = Control Bits for pM[0]
            pM[2] = HEX2
            pM[3] = Control Bits for pM[1]
			
So when the user wants to go to VPN 2, i need to multiply this by 2 to get its position in the physical Memory

Display a prompt which allows the user to enter any virtual memory address in your system, in hexadecimal form.
