//Files
#include "../lib/constants.h"

//Directories
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

//Location: https://www.tutorialspoint.com/c_standard_library/c_function_rand.html
int getRandomValue(int minRange, int maxRange)
{
    // Prints random number between minRange and maxRange
    //Location: https://www.geeksforgeeks.org/generating-random-number-range-c/
    int randomNumber = (rand() % (maxRange - minRange + 1)) + minRange;	
    
	return randomNumber;
}

void seedRandomNumberGenerator()
{
    time_t t;
    // Initializes randon number generator
	srand((unsigned) time(&t));
}

void findPhysicalMemoryAddressData(char *memory, unsigned short input)
{
    unsigned short offsetMask = 0x00FF;
	unsigned short pageNumber = input >> 8;
	unsigned short pageOffset = input & offsetMask;

    printf("%s\n", "Step 1: Bitshifting user input by 8 to access the page number");
    printf("%s\n", "Step 2: Accessing page offset using & operation with offset mask of 0x00FF");
    printf("%s\n", "Step 3: Finding page number in page table using page number above");
    printf("%s\n", "Step 4: Adding page offset to the frames position in physical memory");
    printf("%s\n", "Step 5: Searching for memory address");
    printf("%s\n", "Step 6: Printing");

    unsigned char frameNum = memory[pageNumber*2];
    int frameNumInPageTable = frameNum;
    int physicalMemoryFramePos = frameNumInPageTable * 256;
    int physicalMemoryOffsetPos = physicalMemoryFramePos + pageOffset;

    
    printf("\n%-8s  | %-8s| %-8s| %-8s\n", "Address", "Frame", "Content", "Data");
    printf("-----------------------------------------------------------------------\n");
    printf("0x%-8X| %-8d| %-8c| ", physicalMemoryOffsetPos, physicalMemoryFramePos, memory[physicalMemoryOffsetPos]);
    printf("%-8s\n\n", "[some info to indicate used addresses]");
}

void writeMemoryToFile(char *memory, FILE *fp)
{
    int frame = 0;
    fprintf(fp, "%-8s  | %-8s| %-8s| %-8s\n", "Address", "Frame", "Content", "Data");
    fprintf(fp, "-----------------------------------------------------------------\n");
    for(unsigned int i = 0; i < PHYSICAL_MEMORY_SIZE ; ++i)
    {
        if(i % 256 == 0 && i != 0)
        {
            frame++;
        }

        fprintf(fp, "0x%-8X| %-8d| %-8c| ", i, frame, memory[i]);
        //fprintf(fp, "%-8s\n", "[some info to indicate used addresses]");
        /*if(memory[i] = "~")
        {
            fprintf(fp, "%-8s\n", "This memory is not in use");
        }
        else
        {
            fprintf(fp, "%-8s\n", "This memory is in use");
        }*/        
    }
}

void InitializePhysicalMemory(char *memory)
{
    for(unsigned int i = 0; i < PHYSICAL_MEMORY_SIZE ; ++i)
    {
        memory[i] = (char) TILDE_ASCII_VALUE;
    }
}