#ifndef CONSTANTS
#define CONSTANTS

static int PHYSICAL_MEMORY_SIZE = 65536;

static int MIN_ASCII_VALUE = 33;
static int MAX_ASCII_VALUE = 125;
static int TILDE_ASCII_VALUE = 126;

static int MIN_BYTES_WRITE = 2048;
static int MAX_BYTES_WRITE = 20480;  //20480

static int FIRST_FRAME_POS = 2;     //First two frames are used for page table
static int LAST_FRAME_POS = 255;    //last frame of physical memory

#endif
