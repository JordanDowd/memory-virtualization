#ifndef MY_FUNCTIONS
#define MY_FUNCTIONS

int getRandomValue(int, int);
void seedRandomNumberGenerator();
void findPhysicalMemoryAddressData(char *, unsigned short);
void writeMemoryToFile(char *, FILE *fp);
void InitializePhysicalMemory(char *);

#endif
