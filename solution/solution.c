//Directories
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>

//Files
#include "../lib/my-functions.h"
#include "../lib/constants.h"

//The following signal hanlder code is referenced
//Location: https://stackoverflow.com/questions/26965508/infinite-while-loop-and-control-c
volatile sig_atomic_t stop;

void intHand(int signum)
{
    stop = 1;
}

int main(int argc, char **argv)
{
    //Signal Handler for CRTL+C
    signal(SIGINT, intHand);

    //physical_memory.txt file
    FILE *fp;
    fp = fopen("./data/physical_memory.txt", "w");

    if (fp == NULL)
    {
        printf("There was a problem opening the file");
        return 2;
    }

    //Seed for Randomness
    seedRandomNumberGenerator();
    
    //Creating the physical memory
    /* The physical memory is 512 bytes. i store the hex value in 
    the first byte and control bits in second byte
    Example:    pM[0] = HEX1
                pM[1] = Control Bits for pM[0]
                pM[2] = HEX2
                pM[3] = Control Bits for pM[1]
    So when the user wants to go to VPN 2, i need to multiply this by 2 to get its position
    in the physical Memory*/
	char *physicalMemory = malloc(PHYSICAL_MEMORY_SIZE);

    if (physicalMemory == NULL) 
    {
        printf("There was a problem allocating the requested memory. Exiting. \n");
        return 1;
    }

    //Fill memory with a unique char (Tilde)
    InitializePhysicalMemory(physicalMemory);
    
    //Generating a random number using a seed
    //Frame is 256 bytes & 256 frames in physical memory
	int frameNumber = getRandomValue(FIRST_FRAME_POS, LAST_FRAME_POS);
    int frameNumberStartingPos = frameNumber * 256; 
    //This is the position where i beign writing bytes in physical memory

    //Generating random number of bytes to write
    int bytesToWrite = getRandomValue(MIN_BYTES_WRITE, MAX_BYTES_WRITE);

    //Printing to CMD
    //printf("Frame Number: %d\n", frameNumber);
    //printf("Frame Position in memory: %d\n", frameNumberStartingPos);
    //printf("Bytes to Write: %d\n", bytesToWrite);
    //printf("\n");

    //Begin writing data into physical memory
    char randomChar;
    int pageTableIndex = 0;
    for (int i = frameNumberStartingPos; i < bytesToWrite + frameNumberStartingPos; ++i)
    {
        if(i % 256 == 0)
        {
            int frame = i / 256;
            unsigned char a = frame;
            //printf("Writing in frame %d\n", frame);
            //printf("%s\n", "Updating Page Table");
            physicalMemory[pageTableIndex] = a;
            pageTableIndex = pageTableIndex + 2;
        }

        randomChar = (char) getRandomValue(MIN_ASCII_VALUE, MAX_ASCII_VALUE);
        physicalMemory[i] = randomChar;
    }

    //print data to txt
    writeMemoryToFile(physicalMemory, fp);

    stop = 0;
    while (!stop)
    {
        //User Input
        printf("\nPlease enter a address:\n");
        unsigned short memoryAddressInput;
	    scanf("%hX", &memoryAddressInput); //address of memoryAddressInput	
        printf("\n");

        //find the user memory address
        findPhysicalMemoryAddressData(physicalMemory, memoryAddressInput);
    }

    printf("\nExiting\n");
                   
    //Close file
    fclose(fp);
    //Free the heap memory
    free(physicalMemory);
    
	return 0;
}